import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Calendar;




//########################################################################################################
//# Class Type : Main Class
//# Class Name : ScriptExecutorClass
//#------------------------------------------------------------------------------------------------------
//# Description: This is driver class or framework flow . Every Test Case has same approach for Data 
//#	initialize , Execution of Test script and at the end Publish test reports so we have created a driver class.  
//#------------------------------------------------------------------------------------------------------
//# Argument : NA
//# BrowserName - NA
//#------------------------------------------------------------------------------------------------------
//# Owner:  Pravesh Jha
//# Created on: 23-Feb-2014
//#------------------------------------------------------------------------------------------------------
//# Reviewer: 
//# Review Date: 
//#------------------------------------------------------------------------------------------------------
//# History Notes: 
//#------------------------------------------------------------------------------------------------------
//# Example :
//#------------------------------------------------------------------------------------------------------
//# Return type : void
//########################################################################################################

public class ScriptExecutorClass extends TestScriptClass
{
	public static void main(String[] args)
	{
		boolean blnFlag = false;
		
		try
		{



			//############ Creating a Object for Generic Class ########################################
			
			GenericClass gen = new GenericClass();

			//############ Get Current directory and Assign Folder Path ###############################
			
			blnFlag = gen.GetCurrentDirectoryandAssignFolderPath();
			
			if(!blnFlag)
			{
				return;
			}
			
			//############ Assign Config Value in Dictionary key ########################################
			
			String ConfigPath = GenericClass.dicInput.get("DataSheetPath")+"Config.xlsx";
			
			blnFlag = gen.ReadConfigData(ConfigPath);
			
			if(!blnFlag)
			{
				return;
			}
			
			//############ Assign Common Value in Dictionary key ########################################

			String Env = GenericClass.dicConfig.get("Environment");

			String DataSheetPath = GenericClass.dicInput.get("DataSheetPath")+"MasterTestData_"+Env+".xlsx";
			
			blnFlag = gen.ReadCommonValueData(DataSheetPath);
			
			if(!blnFlag)
			{
				return;
			}
			
			//############ Get Test suite Sheet Count ########################################


			String TestSuitePath = GenericClass.dicInput.get("TestSuitePath")+"TestSuite.xlsx";
			
			int SheetCount = gen.GetWorkSheetCount(TestSuitePath);
			
			if(SheetCount ==0)
			{
				return;
			}
			
			
			//############ Get Test suite Sheet Count ########################################

			for(int i=0;i<SheetCount;i++)
			{
				String SheetName = gen.GetWorkSheetName(TestSuitePath,i);
				
				//############ Verify WorkSheet Name is not blank ########################################
				
				if(SheetName.trim() != "")
				{
					
					//############ Get Total Execution Row Count from WorkSheet ########################################

					int ExecutionRow = gen.GetTotalRowExecutionCount(TestSuitePath,SheetName,"Run","Y");
					
					if(ExecutionRow != 0)
					{
						for(int j=0;j<ExecutionRow;j++)
						{
							//############  Save Test Suite Data in  ###################################################
							
							blnFlag = gen.SaveTestSuiteValue(TestSuitePath,SheetName,j);
							
							if(!blnFlag)
							{
								break;
							}

							//############ Assign Total Test Iteration Count for Each Test Case ########################################
							
							int TotalScriptCount = gen.TotalIterationCountForSingleTestCase(DataSheetPath,SheetName,GenericClass.dicTestSuite.get("TestCaseID"));
							
							//############ Creating Loop for Test Count ########################################
							
							for(int k=0;k<TotalScriptCount;k++)
							{
								int RowNumber = gen.GetRowNumberbasedonIterationValue(k);

								//############ Add Test Data For Application ########################################
								
								
								blnFlag = gen.AddTestDataForApplication1(GenericClass.dicTestSuite.get("TestCaseID"),DataSheetPath,SheetName,RowNumber);
								if(!blnFlag)
								{
									break;
								}
								
								//############ Create PreReport  ##################################################################################################################################################################
		
								blnFlag = gen.CreateHTMlHeader(SheetName,Integer.parseInt(GenericClass.dicTestSuite.get("TestCaseID")),GenericClass.dicConfig.get("ReportingFormat"),GenericClass.dicTestData.get("TestCaseName"));
								
								if(!blnFlag)
								{
									break;
								}
								
								//################################################################################################
								CallingTestScriptMethod(GenericClass.dicTestSuite.get("AutomatesTestCaseName"));
								gen.CreatingRepotingPackage(GenericClass.dicTestSuite.get("TestCaseID"), GenericClass.dicTestData.get("TestCaseName"));
								gen.EndTestCase();
								if(k == TotalScriptCount -1)
								{
									gen.UpdateTestSuiteWorkBookWithStatus(TestSuitePath,SheetName,j,TotalScriptCount);
								}
								gen.RemoveDictionaryKey();
							//############ Close Test Case ########################################
							}
							GenericClass.TotalFailStepCount = 0;
						//############ End of for Loop To verify Total Instance of DataSheet ###################################
						}
					//############ End of if Statement To verify Execution Row Count against Each WorkSheet ########################
					}
				//############ End of If Statement if WorkSheet Name is blank ######################################################
				}
			//############ End of Test Suite WorkSheet Iteration ###################################################################
			}
			
			gen.CreateTestSuiteSummaryReports("TestSuiteSummary");
			
			System.out.println("Test Suite Execution Complete");
			//############ Close Test Case #########################################################################################
			
		}
		catch(Exception e)
		{
		}
		
	}

	public static void CallingTestScriptMethod(String MethodName)
	{
		try
		{
		//TestScriptClass Script = new TestScriptClass();
			Method method = TestScriptClass.class.getMethod(MethodName); 
			TestScriptClass instance = new TestScriptClass();
			method.invoke(instance);
		}
		catch(Exception e)
		{}
	}
	
}
