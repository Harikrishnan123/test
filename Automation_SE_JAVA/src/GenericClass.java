import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.InetAddress;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Hashtable;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMessage.RecipientType;
import javax.mail.internet.MimeMultipart;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.LineIterator;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.xssf.usermodel.XSSFCreationHelper;
import org.apache.poi.xssf.usermodel.XSSFHyperlink;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.interactions.HasInputDevices;
import org.openqa.selenium.interactions.Mouse;
import org.openqa.selenium.internal.Locatable;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.support.ui.Select;



//########################################################################################################
//# Class Type : Generic Class
//# Class Name : Generic Class
//#------------------------------------------------------------------------------------------------------
//# Description: Generic class is collection of all kind of function which is globally accessible for  
//#	 all kind of application . This is application independent framework.
//#------------------------------------------------------------------------------------------------------
//# Argument : NA
//# BrowserName - NA
//#------------------------------------------------------------------------------------------------------
//# Owner:  Pravesh Jha
//# Created on: 23-Feb-2014
//#------------------------------------------------------------------------------------------------------
//# Reviewer: 
//# Review Date: 
//#------------------------------------------------------------------------------------------------------
//# History Notes: 
//#------------------------------------------------------------------------------------------------------
//# Example :
//#------------------------------------------------------------------------------------------------------
//# Return type : NA
//########################################################################################################

public class GenericClass {
    // ########################################################################################################
    // ### Static and Dictionary key that are used in globally (Function level,
    // Script Level)###################

    public static String ErrDescription;
    public static String strParentObjectName = "";
    public static Hashtable<String, String> dicInput = new Hashtable<String, String>();
    public static Hashtable<String, String> dicConfig = new Hashtable<String, String>();
    public static Hashtable<String, String> dicOutput = new Hashtable<String, String>();
    public static Hashtable<String, String> dicCommonValue = new Hashtable<String, String>();
    public static Hashtable<String, String> dicTestData = new Hashtable<String, String>();
    public static Hashtable<String, String> dicTestSuite = new Hashtable<String, String>();
    public static WebDriver driver = null;
    public static WebElement element = null;
    public static List<WebElement> elements;
    public static FileOutputStream oReportWriter;
    public static Writer HtmlWriter;
    public static String sReportFileName = "";
    public static int iStepCount = 0;
    public static int iPassStepCount = 0;
    public static int iFailStepCount = 0;
    public static int TotalFailStepCount = 0;
    public static String sSscreenshotFile = "";
    public static int IterationCount = 1;
    public static String StartDateExecution = "";
    public static String EndDateExecution = "";

    // ########################################################################################################

    public boolean GetCurrentDirectoryandAssignFolderPath() {

	String Dir = "";
	// Delete Temp Results
	DeleteDirectory(GetTempPath() + "AUT_Results\\");
	DeleteSpecificTextFile();
	try {
	    // #---------- Get Current
	    // Directory---------------------------------
	    Dir = System.getProperty("user.dir");

	    Dir = Dir + "\\src";

	    // #---------- Assign DataSheet
	    // Path---------------------------------

	    dicInput.put("DataSheetPath", Dir + "\\DataSheets\\");

	    // Value = dicInput.get("DataSheetPath");
	    // System.out.println(Value);
	    // #---------- Assign Report Path---------------------------------

	    dicInput.put("ReportPath", Dir + "\\Reports\\");

	    // #---------- Assign Miscellaneous
	    // Path---------------------------------

	    dicInput.put("MiscPath", Dir + "\\Miscellaneous\\");

	    // #---------- Assign Test Suite
	    // Path---------------------------------

	    dicInput.put("TestSuitePath", Dir + "\\TestSuite\\");

	    return true;
	} catch (Exception e) {
	    ErrDescription = "Unable to Get Current Directory path.";
	}
	return false;

    }

    public boolean ReadConfigData(String FilePath) {
	killExcel();
	try {
	    Path path = Paths.get(FilePath);

	    if (Files.exists(path)) {
		try {
		    FileInputStream file = new FileInputStream(new File(FilePath));
		    XSSFWorkbook workbook = new XSSFWorkbook(file);
		    XSSFSheet ws = workbook.getSheet("Config");
		    int rowNum = ws.getLastRowNum() + 1;
		    for (int i = 0; i < rowNum; i++) {
			try {
			    XSSFRow row = ws.getRow(i);
			    dicConfig.put(row.getCell(0).getStringCellValue().trim(), row.getCell(1).getStringCellValue().trim());
			} catch (Exception e) {
			}
		    }
		    workbook.close();
		    file.close();
		    return true;
		} catch (Exception e) {
		    ErrDescription = "Cell format of the worksheet 'Config' is not correct please select format type text under following location- "
			    + FilePath;
		    return false;
		}
	    } else {
		ErrDescription = "Config.xls workbook is not found on following location - " + FilePath;
		return false;
	    }

	} catch (Exception ex) {
	}
	return false;

    }

    public boolean ReadCommonValueData(String FilePath) {
	killExcel();
	try {
	    Path path = Paths.get(FilePath);

	    if (Files.exists(path)) {
		try {
		    FileInputStream file = new FileInputStream(new File(FilePath));

		    XSSFWorkbook workbook = new XSSFWorkbook(file);
		    XSSFSheet ws = workbook.getSheet("CommonValues");
		    int rowNum = ws.getLastRowNum() + 1;
		    for (int i = 0; i < rowNum; i++) {
			try {
			    XSSFRow row = ws.getRow(i);
			    dicCommonValue.put(row.getCell(0).getStringCellValue().trim(), row.getCell(1).getStringCellValue().trim());
			} catch (Exception e) {
			}
		    }
		    workbook.close();
		    file.close();
		    return true;
		} catch (Exception e) {
		    ErrDescription = "Cell format of the worksheet 'CommonValues' is not correct please select format type text under following location- "
			    + FilePath;
		    return false;
		}

	    } else {
		ErrDescription = "Master Test Data workbook is not found on following location - " + FilePath;
		return false;
	    }

	} catch (Exception ex) {
	}
	return false;

    }

    public boolean AddTestDataForApplication(String TestId, String FilePath, String WorkSheetName, int RowNUmber) {
	dicTestData.clear();
	try {
	    Path path = Paths.get(FilePath);

	    if (Files.exists(path)) {
		try {
		    FileInputStream file = new FileInputStream(new File(FilePath));

		    XSSFWorkbook workbook = new XSSFWorkbook(file);
		    XSSFSheet ws = workbook.getSheet(WorkSheetName);
		    int noOfColumns = ws.getRow(0).getPhysicalNumberOfCells();
		    for (int i = 0; i < noOfColumns; i++) {
			try {
			    dicTestData.put(ws.getRow(0).getCell(i).getStringCellValue().trim(), ws.getRow(RowNUmber).getCell(i).getStringCellValue().trim());
			} catch (Exception e) {
			}
		    }
		    workbook.close();
		    file.close();
		    return true;
		} catch (Exception e) {
		    ErrDescription = "Cell format of the worksheet 'CommonValues' is not correct please select format type text under following location- "
			    + FilePath;
		    return false;
		}

	    } else {
		ErrDescription = "Master Test Data workbook is not found on following location - " + FilePath;
		return false;
	    }

	} catch (Exception ex) {
	}
	return false;

    }

    public boolean OpenBrowser(String strapplicationURL, String Browser) {
	boolean blnFlag = false;
	String ThirdPartyexe = dicInput.get("MiscPath") + "BrowserDriver\\";
	try {
	    switch (Browser.toString().trim().toLowerCase()) {
	    case "firefox":
	    case "ff":
	    case "mozila":
		driver = new FirefoxDriver();
		break;
	    case "chrome":
	    case "googlechrome":
		DesiredCapabilities capability = DesiredCapabilities.chrome();
		ChromeOptions options = new ChromeOptions();
		options.addArguments(new String[] { "test-type" });
		capability.setCapability(ChromeOptions.CAPABILITY, options);
		File file = new File(ThirdPartyexe + "chromedriver.exe");
		System.setProperty("webdriver.chrome.driver", file.getAbsolutePath());
		driver = new ChromeDriver(options);
		break;
	    case "ie":
	    case "iexplorer":
	    case "internetexplorer":
		DesiredCapabilities capabilities = DesiredCapabilities.internetExplorer();
		capabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
		File file1 = new File(ThirdPartyexe + "IEDriverServer.exe");
		System.setProperty("webdriver.ie.driver", file1.getAbsolutePath());
		driver = new InternetExplorerDriver(capabilities);
		break;
	    case "safari":
		driver = new SafariDriver();
		break;
//	    case "opera":
//		OperaProfile profile = new OperaProfile(); // fresh, random
//							   // profile
//		profile.preferences().set("User Prefs", "Ignore Unrequested Popups", false);
//		DesiredCapabilities opera = DesiredCapabilities.opera();
//		opera.setCapability("opera.profile", profile);
//		driver = new OperaDriver();
//		break;

	    }
	} catch (Exception e) {
	    ErrDescription = "Unable to Launch Browser - " + Browser + " driver.";
	    return false;
	}

	// @ Launch Application
	try {
	    blnFlag = ExecuteActionOnBrowser("Navigate", strapplicationURL);
	} catch (Exception e) {
	    ErrDescription = "Unable to Launch Application - " + strapplicationURL + "  on Browser - " + Browser + ".";
	    return false;
	}

	// @ Get Parent Window ID
	blnFlag = ExecuteActionOnBrowser("windowid", "");
	dicOutput.put("ParentWinID", dicOutput.get("strOutPut"));
	return blnFlag;
    }

    public boolean ExecuteActionOnBrowser(String strBrowserAction, String strValue) {

	boolean Flag = false;

	dicOutput.put("strOutPut", "");
	try {
	    switch (strBrowserAction.toLowerCase().trim().replace(" ", "")) {
	    case "windowid":
		dicOutput.put("strOutPut", driver.getWindowHandle().toString());
		return true;
	    case "back":
		driver.navigate().back();
		return true;
	    case "forward":
		driver.navigate().forward();
		return true;
	    case "refresh":
		driver.navigate().refresh();
		return true;
	    case "maximize":
		driver.manage().window().maximize();
		return true;
	    case "geturl":
		dicOutput.put("strOutPut", driver.getCurrentUrl().toString());
		return true;
	    case "navigate":
		driver.navigate().to(strValue);
		driver.manage().window().maximize();
		return true;
	    case "gettitle":
		dicOutput.put("strOutPut", driver.getTitle().toString());
		return true;
	    case "focus":
		switch (strValue.toLowerCase().trim().replace(" ", "")) {
		case "0":
		case "":
		    driver.switchTo().window(dicOutput.get("ParentWinID"));
		    driver.manage().window().maximize();
		    WaitForPageLoad(60);
		    return true;
		case "1":
		    String parentId = driver.getWindowHandles().iterator().next();
		    String ChildId = driver.getWindowHandles().iterator().next();
		    driver.switchTo().window(ChildId);
		    driver.manage().window().maximize();
		    WaitForPageLoad(60);
		    return true;
		case "2":
		    String parentId1 = driver.getWindowHandles().iterator().next();
		    String ChildId1 = driver.getWindowHandles().iterator().next();
		    String ChildId2 = driver.getWindowHandles().iterator().next();
		    driver.switchTo().window(ChildId2);
		    driver.manage().window().maximize();
		    WaitForPageLoad(60);
		    return true;
		}
		return true;
	    case "basewindow":
		driver.switchTo().window(dicOutput.get("ParentWinID"));
		return true;
	    case "close":
		driver.close();
		return true;
	    case "getpagesource":
		dicOutput.put("strOutPut", driver.getPageSource().toString());
		return true;
	    case "count":
		dicOutput.put("strOutPut", Integer.toString(driver.getWindowHandles().size()));
		return true;

	    }

	} catch (Exception ee) {
	    ErrDescription = "Unbale to Perform Action : " + strBrowserAction + " On Browser";
	}
	return Flag;
    }

    public boolean ExecuteActionOnBrowser(String strBrowserAction) {
	return ExecuteActionOnBrowser(strBrowserAction, "");
    }

    public void EndTestCase() {
	DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
	Calendar calobj = Calendar.getInstance();
	EndDateExecution = df.format(calobj.getTime()).toString();
	CreateHTMLFooterReport();
	// @ get all Open Browser instance.
	int TotalBrower = 0;
	ExecuteActionOnBrowser("count", "");
	TotalBrower = Integer.parseInt(dicOutput.get("strOutPut"));

	// @ focus on Browser and close the browser
	for (int i = 0; i <= TotalBrower; i++) {
	    ExecuteActionOnBrowser("focus", String.valueOf(i));
	    ExecuteActionOnBrowser("close");
	}

	// @ close driver instance
	try {
	    driver.quit();
	} catch (Exception e) {
	}

    }

    public void RemoveDictionaryKey() {
	// dicTestSuite.clear();
	iStepCount = 0;
	iPassStepCount = 0;
	iFailStepCount = 0;
	dicTestData.clear();
	dicOutput.clear();
	StartDateExecution = "";
	EndDateExecution = "";
    }

    public boolean ExecuteActionOnPage(String strPageName, String strAction, String strObjectProperty, String strValue, int indexValue) {
	// @ ######## Assign Variable #####################
	boolean blnFlag = false;
	dicOutput.put("strOutPut", "");
	ErrDescription = "";

	// @ ######## Check Object Property ####################

	if (strObjectProperty.trim().equals("")) {
	    ErrDescription = "Object property is null on script section.";
	    return false;
	}

	// @ Get Object Property and break this property into Xpath.

	String XpathProperty = ConvertObjectPropertyintoXPATH(strObjectProperty, indexValue);

	if (XpathProperty.trim() == "") {
	    ErrDescription = "Object property should not be blank.";
	    return false;
	}

	// @ Verify Element Existence

	blnFlag = ElementExistenceCheck(XpathProperty);
	if (!blnFlag) {
	    ErrDescription = "Element - " + strObjectProperty + " does not exist.";
	    return false;
	}

	// @ Perform Action.
	Actions action = new Actions(driver);

	switch (strAction.toLowerCase().trim().replace(" ", "")) {
	case "click":
	    element.click();
	    WaitForPageLoad(60);
	    return true;
	case "exist":
	    return element.isDisplayed();
	case "rightclick":
	    action.contextClick(element).perform();
	    return true;
	case "set":
	    element.clear();
	    element.sendKeys(strValue);
	    return true;
	case "isselected":
	    return element.isSelected();
	case "uncheck":
	    if (element.isSelected()) {
		element.click();
		return true;
	    }
	    break;
	case "selectbytext":
	    Select sel = new Select(element);
	    sel.selectByVisibleText(strValue);
	    return true;
	case "selectbyvalue":
	    Select sel1 = new Select(element);
	    sel1.selectByValue(strValue);
	    return true;
	case "selectbyindex":
	    Select sel2 = new Select(element);
	    sel2.selectByIndex(Integer.parseInt(strValue));
	    return true;
	case "deselectAll":
	    Select sel3 = new Select(element);
	    sel3.deselectAll();
	    return true;
	case "innerhtml":
	    String Value = ((JavascriptExecutor) driver).executeScript("return arguments[0].innerHTML;", element).toString();
	    dicOutput.put("strOutPut", Value);
	    ((JavascriptExecutor) driver).executeScript("return arguments[0].innerHTML;", element).toString();
	    return true;
	case "innertext":
	    String Value1 = ((JavascriptExecutor) driver).executeScript("return arguments[0].innerText;", element).toString();
	    dicOutput.put("strOutPut", Value1);
	    return true;
	case "mousehover":
	    return MouseOverandHold();
	case "doubleclick":
	    action.doubleClick(element).perform();
	    return true;
	case "splclick":
	    action.click(element).build().perform();
	    WaitForPageLoad(60);
	    return true;
	case "focus":
	    action.moveToElement(element).build().perform();
	    return true;
	case "clickontooltip":
	    String TargetElement = ConvertObjectPropertyintoXPATH(strValue, indexValue);
	    WebElement Telement = driver.findElement(By.xpath(TargetElement));
	    action.dragAndDrop(element, Telement).click().build().perform();
	    return true;
	case "drag":
	    action.moveToElement(element).dragAndDrop(element, element).perform();
	    return true;
	case "move":
	    action.moveToElement(element).click().build().perform();
	    return true;
	case "highlight":
	    return elementHighlight(element);
	case "count":
	    String strOjProperty = ConvertObjectPropertyintoXPATH(strObjectProperty, indexValue);
	    if (strValue != "") {
		strOjProperty = strOjProperty + strValue;
	    }
	    elements = driver.findElements(By.xpath(strOjProperty));
	    try {
		dicOutput.put("strOutPut", String.valueOf(elements.size()));
	    } catch (Exception e) {
		dicOutput.put("strOutPut", "0");
		return false;
	    }
	    return true;
	case "getcoordinate":
	    dicOutput.put("strOutPut", String.valueOf(element.getLocation()));
	    return true;
	case "getcssvalue":
	    dicOutput.put("strOutPut", element.getCssValue(strValue));
	    return true;
	case "getattribute":
	    dicOutput.put("strOutPut", element.getAttribute(strValue));
	    return true;
	case "gettext":
	    dicOutput.put("strOutPut", element.getText());
	    return true;
	case "gettagname":
	    dicOutput.put("strOutPut", element.getTagName());
	    return true;
	}

	return true;
    }

    public boolean ExecuteActionOnPage(String strPageName, String strAction, String strObjectProperty, String strValue) {
	return ExecuteActionOnPage(strPageName, strAction, strObjectProperty, strValue, 1);
    }

    public boolean ExecuteActionOnPage(String strPageName, String strAction, String strObjectProperty) {
	return ExecuteActionOnPage(strPageName, strAction, strObjectProperty, "", 1);
    }

    public boolean VerifyObjectText(String ObjectProperty, String TextToMatch, int Index) {
	WaitForObject("pageelement", ObjectProperty, 10);
	ExecuteActionOnPage("pageelement", "gettext", ObjectProperty, "", Index);
	return MatchSubstringFromString(dicOutput.get("strOutPut"), TextToMatch);
    }

    public boolean VerifyObjectText(String ObjectProperty, String TextToMatch) {
	return VerifyObjectText(ObjectProperty, TextToMatch, 1);
    }

    public boolean MatchSubstringFromString(String set, String subset) {
	if (set.toLowerCase().trim().contains(subset.toLowerCase().trim())) {
	    return true;
	} else {
	    return false;
	}
    }

    public boolean MatchFullString(String set, String subset) {
	if (set.toLowerCase().trim().equals((subset.toLowerCase().trim()))) {
	    return true;
	} else {
	    return false;
	}
    }

    public String ConvertObjectPropertyintoXPATH(String strObjectProperty, int indexValue) {
	String XPath = "";
	String Prefix = "";
	String Suffix = "";
	try {
	    Prefix = strObjectProperty.substring(0, 4).toLowerCase();
	    Suffix = strObjectProperty.substring(4);
	} catch (Exception e) {
	}

	switch (Prefix.toLowerCase()) {
	case "idef":
	    XPath = strParentObjectName + "//*[@id='" + Suffix + "']";
	    break;
	case "idep":
	    XPath = strParentObjectName + "//*[contains(@id,'" + Suffix + "')" + "]";
	    break;
	case "txtp":
	    XPath = strParentObjectName + "//*[contains(text(),'" + Suffix + "')" + "]";
	    break;
	case "txtf":
	    XPath = strParentObjectName + "//*[text()='" + Suffix + "']";
	    break;
	case "clsf":
	    XPath = strParentObjectName + "//*[@class='" + Suffix + "']";
	    break;
	case "clsp":
	    XPath = strParentObjectName + "//*[contains(@class,'" + Suffix + "')" + "]";
	    break;
	case "altf":
	    XPath = strParentObjectName + "//*[@alt='" + Suffix + "']";
	    break;
	case "altp":
	    XPath = strParentObjectName + "//*[contains(@alt,'" + Suffix + "')" + "]";
	    break;
	case "href":
	    XPath = strParentObjectName + "//*[@href='" + Suffix + "']";
	    break;
	case "hrep":
	    XPath = strParentObjectName + "//*[contains(@href,'" + Suffix + "')" + "]";
	    break;
	case "valf":
	    XPath = strParentObjectName + "//*[@value='" + Suffix + "']";
	    break;
	case "valp":
	    XPath = strParentObjectName + "//*[contains(@value,'" + Suffix + "')" + "]";
	    break;
	case "namf":
	    XPath = strParentObjectName + "//*[@name='" + Suffix + "']";
	    break;
	case "namp":
	    XPath = strParentObjectName + "//*[contains(@name,'" + Suffix + "')" + "]";
	    break;
	case "srcf":
	    XPath = strParentObjectName + "//*[@src='" + Suffix + "']";
	    break;
	case "srcp":
	    XPath = strParentObjectName + "//*[contains(@src,'" + Suffix + "')" + "]";
	    break;
	case "typf":
	    XPath = strParentObjectName + "//*[@type='" + Suffix + "']";
	    break;
	case "typp":
	    XPath = strParentObjectName + "//*[contains(@type,'" + Suffix + "')" + "]";
	    break;
	case "forf":
	    XPath = strParentObjectName + "//*[@for='" + Suffix + "']";
	    break;
	case "forp":
	    XPath = strParentObjectName + "//*[contains(@for,'" + Suffix + "')" + "]";
	    break;
	case "oncf":
	    XPath = strParentObjectName + "//*[@onclick='" + Suffix + "']";
	    break;
	case "oncp":
	    XPath = strParentObjectName + "//*[contains(@onclick,'" + Suffix + "')" + "]";
	    break;
	case "lnkf":
	    XPath = strParentObjectName + "//a[text()='" + Suffix + "']";
	    break;
	case "lnkp":
	    XPath = strParentObjectName + "//a[contains(text(),'" + Suffix + "')" + "]";
	    break;
	case "cssf":
	    XPath = strParentObjectName + "//*[@css='" + Suffix + "']";
	    break;
	case "cssp":
	    XPath = strParentObjectName + "//*[contains(@css,'" + Suffix + "')" + "]";
	    break;
	case "titf":
	    XPath = strParentObjectName + "//*[@title='" + Suffix + "']";
	    break;
	case "titp":
	    XPath = strParentObjectName + "//*[contains(@title,'" + Suffix + "')" + "]";
	    break;
	case "rolf":
	    XPath = strParentObjectName + "//*[@role='" + Suffix + "']";
	    break;
	case "rolo":
	    XPath = strParentObjectName + "//*[contains(@role,'" + Suffix + "')" + "]";
	    break;
	default:
	    if (indexValue != 1) {
		strObjectProperty = "(" + strObjectProperty + ")[" + indexValue + "]";
	    }
	    return strObjectProperty;
	}
	if (indexValue != 1) {
	    XPath = "(" + XPath + ")[" + indexValue + "]";
	}
	return XPath;
    }

    public boolean ElementExistenceCheck(String Xpath) {
	try {
	    element = driver.findElement(By.xpath(Xpath));
	    return true;
	} catch (Exception e) {
	    ErrDescription = "element does not exist on page level";
	}
	return false;
    }

    public boolean MouseOverandHold() {
	try {
	    Mouse mouse = ((HasInputDevices) driver).getMouse();
	    Locatable hoverItem = (Locatable) element;
	    mouse.mouseMove(hoverItem.getCoordinates());
	    return true;
	} catch (Exception e) {
	}
	return false;
    }

    public boolean elementHighlight(WebElement element) {
	try {
	    for (int i = 0; i < 5; i++) {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].setAttribute('style', arguments[1]);", element, "color: yellow; border: 5px solid yellow;");
		js.executeScript("arguments[0].setAttribute('style', arguments[1]);", element, "");
		Thread.sleep(300);
	    }
	    return true;
	} catch (Exception e) {
	}
	return false;
    }

    public void WaitForPageLoad(int intTimeOut) {
	boolean blnFlag = false;
	try {
	    for (int i = 0; i < intTimeOut; i++) {
		blnFlag = ((JavascriptExecutor) driver).executeScript("return document.readyState").equals("complete");
		if (blnFlag) {
		    return;
		}
	    }
	    Thread.sleep(2000);
	} catch (Exception e) {
	}
    }

    public boolean WaitForObject(String strPagesection, String objectproperty, int TimeOut) {
	try {
	    boolean blnFlag = false;
	    for (int i = 1; i <= TimeOut; i++) {
		blnFlag = ExecuteActionOnPage(strPagesection, "exist", objectproperty, "", 1);
		if (!blnFlag) {
		    Thread.sleep(1000);
		} else {
		    return true;
		}
	    }
	} catch (Exception e) {
	}
	return false;
    }

    public boolean WaitForObject(String strPagesection, String objectproperty) {
	return WaitForObject(strPagesection, objectproperty, 30);
    }

    public void SaveScreenShot(String ImagePath) {
	try {
	    File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
	    // Now you can do whatever you need to do with it, for example copy
	    // somewhere
	    FileUtils.copyFile(scrFile, new File(ImagePath));
	} catch (Exception e) {
	}
    }

    public boolean ExecuteActionOnFarme(String FrameObjectProperty) {
	boolean blnFlag = false;
	if (FrameObjectProperty.toLowerCase().trim().equals("home")) {
	    driver.switchTo().defaultContent();
	    return true;
	}
	String XPath = ConvertObjectPropertyintoXPATH(FrameObjectProperty, 1);
	blnFlag = ElementExistenceCheck(XPath);
	if (!blnFlag) {
	    ErrDescription = "Frame -: " + FrameObjectProperty + " does not exist on page.";
	    return false;
	}
	try {
	    driver.switchTo().frame(element);
	    return true;
	} catch (Exception e) {
	    ErrDescription = "Frame -: " + FrameObjectProperty + " does not exist on page.";
	    return false;
	}

    }

    public boolean ExecuteActionOnPopup(String strAction) {
	// @ verify Pop up is present or not.
	try {
	    switch (strAction.toLowerCase().trim()) {
	    case "accept":
	    case "ok":
	    case "yes":
		driver.switchTo().alert().accept();
		break;
	    case "dismiss":
	    case "no":
	    case "cancel":
		driver.switchTo().alert().dismiss();
		break;
	    case "gettext":
		dicOutput.put("strOutPut", driver.switchTo().alert().getText());
		break;
	    }
	} catch (Exception e) {
	    dicOutput.put("strOutPut", "");
	    return false;
	}

	return true;
    }

    public boolean CreateFolderStructureForReport(String worksheetname, int testcaseID, String ReportingFormat, String TestName) {
	// ------ Get Current Date and Time--------------
	try {
	    String Env = dicConfig.get("Environment") + "-Environment";
	    DateFormat df = new SimpleDateFormat("dd/MM/yy");
	    Calendar calobj = Calendar.getInstance();
	    String DateFolder = df.format(calobj.getTime()).toString().replace("/", "-");
	    DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
	    Calendar cal = Calendar.getInstance();
	    String TimeFolder = dateFormat.format(cal.getTime()).toString().replace(":", "-");

	    // @ get full Folder Name
	    String HTMLFolder = dicInput.get("ReportPath") + DateFolder + "\\" + Env + "\\" + worksheetname + "\\" + testcaseID + "\\" + TimeFolder + "\\";
	    dicConfig.put("HTMLFolder", HTMLFolder);
	    // @ Create new Folder
	    File file = new File(HTMLFolder);
	    if (!file.exists()) {
		if (file.mkdirs()) {
		    sReportFileName = HTMLFolder + testcaseID + ".html";
		    return true;
		} else {
		    ErrDescription = "Failed to create directory!";
		    return false;
		}
	    }
	} catch (Exception e) {
	    ErrDescription = "Unable to create Reporting Folder structure.";
	    return false;
	}
	return false;
    }

    public boolean CreateHTMlHeader(String worksheetname, int testcaseID, String ReportingFormat, String TestName) {
	boolean blnFlag = false;
	killExcel();
	String Env = dicConfig.get("Environment") + "-Environment";
	blnFlag = CreateFolderStructureForReport(worksheetname, testcaseID, ReportingFormat, TestName);
	if (blnFlag) {
	    try {

		oReportWriter = new FileOutputStream(sReportFileName);
		HtmlWriter = new OutputStreamWriter(oReportWriter);
		HtmlWriter.write("<html>");
		HtmlWriter.write("<style>");
		HtmlWriter
			.write(".subheading { BORDER-RIGHT:#000000 1px solid;PADDING-RIGHT: 4px;BORDER-TOP: #000000 1px solid;PADDING-LEFT: 4px;FONT-WEIGHT: bold;FONT-SIZE: 11pt;PADDING-BOTTOM: 0px;BORDER-LEFT: #000000 1px solid;COLOR: #000000;PADDING-TOP: 0px;BORDER-BOTTOM: #000000 1px solid;FONT-FAMILY: Consolas,helvetica, sans-serif;HEIGHT: 10px;BACKGROUND-COLOR: #FAC090;Color: #000000}");

		HtmlWriter
			.write(".subheading1{BORDER-RIGHT: #000000 1px solid;BACKGROUND-COLOR: #CCC0DA;PADDING-RIGHT: 4px;BORDER-TOP: #000000 1px solid;PADDING-LEFT: 4px;FONT-WEIGHT: bold;FONT-SIZE: 13pt;PADDING-BOTTOM: 0px;BORDER-LEFT: #000000 1px solid;COLOR: #000000;PADDING-TOP: 0px;BORDER-BOTTOM: #000000 1px solid;FONT-FAMILY: Consolas,helvetica, sans-serif;HEIGHT: 10px;}");

		HtmlWriter
			.write(".subheading2{BORDER-RIGHT: #000000 1px solid;PADDING-RIGHT: 2px;BORDER-TOP: #000000 1px solid;PADDING-LEFT: 2px;FONT-WEIGHT: bold;FONT-SIZE: 11pt;PADDING-BOTTOM: 0px;BORDER-LEFT: #000000 1px solid;COLOR: #000000;PADDING-TOP: 0px;BORDER-BOTTOM: #000000 1px solid;FONT-FAMILY: Consolas,helvetica, sans-serif;HEIGHT: 10px;BACKGROUND-COLOR: #C2DC9A;Color: #000000}");

		HtmlWriter
			.write(".tdborder_1{BORDER-RIGHT: #000000 1px solid;PADDING-RIGHT: 4px;BORDER-TOP: #000000 1px solid;PADDING-LEFT: 4px;FONT-SIZE: 11pt;PADDING-BOTTOM: 0px;BORDER-LEFT: #000000 1px solid;COLOR: #000000;PADDING-TOP: 0px;BORDER-BOTTOM: #000000 1px solid;FONT-FAMILY: Consolas, helvetica,  sans-serif;HEIGHT: 10px}");

		HtmlWriter
			.write(".tdborder_1_Pass{BORDER-RIGHT: #000000 1px solid;PADDING-RIGHT: 4px;BORDER-TOP: #000000 1px solid;PADDING-LEFT: 4px;FONT-SIZE: 11pt;PADDING-BOTTOM: 0px;BORDER-LEFT: #000000 1px solid;COLOR: #00ff00;PADDING-TOP: 0px;BORDER-BOTTOM: #000000 1px solid;FONT-FAMILY: Consolas,  helvetica, sans-serif;HEIGHT: 10px}");

		HtmlWriter
			.write(".SnapShotLink_style{PADDING-RIGHT: 4px;PADDING-LEFT: 4px;FONT-SIZE: 11pt;PADDING-BOTTOM: 0px;COLOR: #0000EE;PADDING-TOP: 0px;FONT-FAMILY: Consolas, helvetica, sans-serif;HEIGHT: 10px}");

		HtmlWriter
			.write(".tdborder_1_Fail{BORDER-RIGHT: #000000 1px solid;PADDING-RIGHT: 4px;BORDER-TOP: #000000 1px solid;PADDING-LEFT: 4px;FONT-SIZE: 11pt;PADDING-BOTTOM: 0px;BORDER-LEFT: #000000 1px solid; COLOR: #ff0000;PADDING-TOP: 0px;BORDER-BOTTOM: #000000 1px solid;FONT-FAMILY: Consolas, helvetica, sans-serif;HEIGHT: 10px}");

		HtmlWriter
			.write(".tdborder_1_Done{BORDER-RIGHT: #000000 1px solid;PADDING-RIGHT: 4px;BORDER-TOP: #000000 1px solid;PADDING-LEFT: 4px;FONT-SIZE: 11pt;PADDING-BOTTOM: 0px;BORDER-LEFT: #000000 1px solid; COLOR: #ffcc00;PADDING-TOP: 0px;BORDER-BOTTOM: #000000 1px solid;FONT-FAMILY: Consolas,  helvetica, sans-serif;HEIGHT: 10px}");

		HtmlWriter
			.write(".tdborder_1_Skipped{BORDER-RIGHT: #000000 1px solid;PADDING-RIGHT: 4px;BORDER-TOP: #000000 1px solid;PADDING-LEFT: 4px;FONT-SIZE: 11pt;PADDING-BOTTOM: 0px;BORDER-LEFT: #000000 1px  solid;COLOR: #00ccff;PADDING-TOP: 0px;BORDER-BOTTOM: #000000 1px solid;FONT-FAMILY: Consolas, helvetica, sans-serif;HEIGHT: 10px}");

		HtmlWriter
			.write(".tdborder_1_Warning{BORDER-RIGHT: #000000 1px solid;PADDING-RIGHT: 4px;BORDER-TOP: #000000 1px solid;PADDING-LEFT: 4px;FONT-SIZE: 11pt;PADDING-BOTTOM: 0px;BORDER-LEFT: #000000 1px solid;COLOR: #660066;PADDING-TOP: 0px;BORDER-BOTTOM: #000000 1px solid;FONT-FAMILY: Consolas, helvetica, sans-serif;HEIGHT: 10px}");

		HtmlWriter.write(".heading {FONT-WEIGHT: bold; FONT-SIZE: 17px; COLOR: #005484;FONT-FAMILY: Consolas, Verdana, Tahoma, Consolas;}");

		HtmlWriter
			.write(".style1 { border: 1px solid #8eb3d8;padding: 0px 4px;FONT-WEIGHT: bold;FONT-SIZE: 11pt;COLOR: #000000;FONT-FAMILY: Consolas, helvetica, sans-serif;HEIGHT: 10px;width: 180px;}");

		HtmlWriter
			.write(".style3 { border: 1px solid #8eb3d8;padding: 0px 4px;FONT-WEIGHT: bold;FONT-SIZE: 11pt;COLOR: #000000;FONT-FAMILY: Consolas, helvetica, sans-serif;HEIGHT: 10px;width: 2px;}");

		HtmlWriter.write("</style>");

		HtmlWriter.write("<head><title>" + dicConfig.get("ProjectName") + " Test Result</title></head>");

		HtmlWriter.write("<body>");

		HtmlWriter.write("<table cellSpacing='0' cellPadding='0' border='0' align='center' style='width:96%;"
			+ " margin-left:10px;'><td class='subheading1' colspan=5 align=center><p style='font-size:1.8em'>" + "<body link='#00ff00'>"
			+ dicConfig.get("ProjectName") + " Test Case Report </body></td><tr></tr></table>");
		HtmlWriter.write("<table cellSpacing='0' cellPadding='0' border='0' align='center' style='width:96%; margin-" + "left:10px;'>");

		HtmlWriter.write("<TR>" + " <TD class='subheading2' align='center' >Test Case ID</TD>"
			+ " <TD class='subheading2' align='center' >Test Case Name</TD>" + " <TD class='subheading2' align='center'>Environment</TD>");

		HtmlWriter.write(" <TD class='subheading2'align='center'>Browser</TD>");

		HtmlWriter.write(" <TD class='subheading2' align='center'>Application URL</TD>" + " </TR>");

		HtmlWriter.write("<TR>" + " <TD class='tdborder_1'  vAlign=center  align=middle >" + testcaseID + "</TD>"
			+ " <TD class='tdborder_1'  vAlign=center  align=middle >" + TestName + "</TD>"
			+ " <TD class='tdborder_1'  vAlign=center  align=middle >" + Env + "</TD>");

		HtmlWriter.write(" <TD class='tdborder_1'  vAlign=center  align=middle >" + dicConfig.get("BrowserType") + "<br> Version( " + " Version"
			+ " )</TD>");

		HtmlWriter.write(" <TD class='tdborder_1'  vAlign=center  align=middle >" + dicCommonValue.get("ApplicatonURL") + "</TD>" + " </TR>");

		HtmlWriter.write("</table>");

		HtmlWriter.write("<table cellSpacing='0' cellPadding='0' border='0' align='center' style='width:96%;" + " margin-left:10px;'>");

		HtmlWriter.write("<tr></tr>");

		HtmlWriter.write("<tr>" + " <td class='subheading2' width ='5%' align='center'>Steps</td>"
			+ " <td class='subheading2'  width ='38%' align='center'>Description</td>"
			+ " <td class='subheading2'  width ='38%' align='center'>Actual Result</td>"
			+ " <td class='subheading2' width ='9%' align='center'>Step Status</td>"
			+ " <td class='subheading2' width ='10%' align='center'>SnapShot</td>" + " </tr>");

		return true;
	    } catch (Exception e) {
		ErrDescription = "Unable to create HTML object for report.";
		return false;
	    }
	} else {
	    return false;
	}
    }

    public void ReportStep(String StepDescription, String ActualDescription, String Status, String ScreenhotName) {
	try {
	    sSscreenshotFile = "STEP-" + (iStepCount + 1);
	    sSscreenshotFile = sSscreenshotFile.replace(" ", "");
	    String ImagePath = dicConfig.get("HTMLFolder") + sSscreenshotFile + ".png";
	    ;
	    if (ScreenhotName.trim() != "") {
		sSscreenshotFile = ScreenhotName;
	    }

	    if (dicConfig.get("ScreenShot").toLowerCase().trim().equals("yes")) {
		ExecuteActionOnBrowser("count");
		if (!MatchSubstringFromString(dicOutput.get("strOutPut"), "0")) {
		    SaveScreenShot(ImagePath);
		}
	    }
	    HtmlWriter.write("<tr>");
	    HtmlWriter.write("<TD class='tdborder_1' width ='5%' align='center' >" + ++iStepCount + "</TD>");
	    HtmlWriter.write("<td class ='tdborder_1' width ='38%' >" + StepDescription + "</td>");
	    HtmlWriter.write("<td class ='tdborder_1' width ='38%' >" + ActualDescription + "</td>");
	    if (Status.toLowerCase().trim().equals("pass") || Status.toLowerCase().equals("done") || Status.toLowerCase().trim().equals("warning")) {

		HtmlWriter.write(" <td  class ='tdborder_1_Pass' width ='9%'align='center' >" + Status + "</td>");

		if (dicConfig.get("ScreenShot").toLowerCase().trim().equals("yes")) {
		    HtmlWriter.write(" <td > <a class ='tdborder_1_Warning' width ='12%' align='center' href=" + ImagePath + ">Refer Screenshot</a></TD>");
		} else {
		    HtmlWriter.write(" <td class ='tdborder_1' width ='10%' align='center'> </td>");
		}
		iPassStepCount += 1;
	    } else {
		if (dicConfig.get("ScreenShot").toLowerCase().trim().equals("no")) {
		    ExecuteActionOnBrowser("count");
		    if (!MatchSubstringFromString(dicOutput.get("strOutPut"), "0")) {
			SaveScreenShot(ImagePath);
		    }
		}
		HtmlWriter.write(" <td  class ='tdborder_1_Fail' width ='9%' align='center' >" + Status + "</td>");
		HtmlWriter.write(" <td > <a class ='tdborder_1_Warning' width ='12%' align='center' href=" + ImagePath + ">Refer Screenshot</a></TD>");
		iFailStepCount += 1;
	    }
	    HtmlWriter.write("</tr>");

	} catch (Exception e) {
	}
	TotalFailStepCount = TotalFailStepCount + iFailStepCount;
    }

    public void ReportStep(String StepDescription, String ActualDescription, String Status) {
	ReportStep(StepDescription, ActualDescription, Status, "");
    }

    public void CreateHTMLFooterReport() {
	try {
	    HtmlWriter.write("</table>");

	    HtmlWriter.write("<table cellSpacing='0' cellPadding='0' border='0' align='center' style='width:96%; " + " margin-left:10px;'>");

	    HtmlWriter.write("<TR></TR>");

	    HtmlWriter.write("<TR>");
	    HtmlWriter.write("<TD class='subheading2' align='center'>Test Step-Pass</TD>");
	    HtmlWriter.write("<TD class='subheading2' align='center'>Test Step-Fail</TD>");
	    HtmlWriter.write("<TD class='subheading2' align='center'>Execution date & time</TD>");
	    HtmlWriter.write("<TD class='subheading2' align='center'>Execution Machine name</TD>");
	    HtmlWriter.write("<TD class='subheading2' align='center'>Test run duration</TD>");
	    HtmlWriter.write("<TD class='subheading2' align='center'>Language</TD>");

	    HtmlWriter.write("</TR>");

	    HtmlWriter.write("<TR>");
	    HtmlWriter.write("<TD class='tdborder_1'  align='center' >" + iPassStepCount + "</TD>");
	    HtmlWriter.write("<TD class='tdborder_1'  align='center'>" + iFailStepCount + "</TD>");
	    HtmlWriter.write("<TD class='tdborder_1'  align='center'>" + StartDateExecution + "</TD>");
	    HtmlWriter.write("<TD class='tdborder_1'  align='center'>" + InetAddress.getLocalHost().getHostName() + "</TD>");

	    HtmlWriter.write("<TD class='tdborder_1' align='center' >" + GetTimeDiffernce(StartDateExecution, EndDateExecution) + "</TD>");
	    HtmlWriter.write("<TD class='tdborder_1' align='center' >" + "JAVA" + "</TD>");
	    HtmlWriter.write("</TR>");
	    HtmlWriter.write("<tr></tr><tr></tr><tr></tr><tr></tr><tr></tr><tr></tr>");

	    HtmlWriter.write("</table>");
	    HtmlWriter.close();
	    oReportWriter.close();
	} catch (Exception e) {
	    return;
	}
    }

    public int GetWorkSheetCount(String WorkSheetPath) {
	killExcel();
	int TotalWorkSheet = 0;
	Path path = Paths.get(WorkSheetPath);
	if (Files.exists(path)) {
	    try {
		FileInputStream file = new FileInputStream(new File(WorkSheetPath));

		XSSFWorkbook workbook = new XSSFWorkbook(file);
		TotalWorkSheet = workbook.getNumberOfSheets();
		workbook.close();
		file.close();
		return TotalWorkSheet;
	    } catch (Exception e) {
		ErrDescription = "Unable to get worksheet count from Testsuite.xlsx file";
	    }
	} else {
	    ErrDescription = "Test Suite workbook is not found on following location - " + WorkSheetPath;
	}
	return TotalWorkSheet;
    }

    public int GetTotalRowExecutionCount(String WorkSheetPath, String WorkSheetName, String ColumnName, String ColumnValue) {
	killExcel();
	Path path = Paths.get(WorkSheetPath);
	int i = 0;
	int TotalExecutionCount = 0;
	if (Files.exists(path)) {
	    try {
		FileInputStream file = new FileInputStream(new File(WorkSheetPath));
		XSSFWorkbook workbook = new XSSFWorkbook(file);
		XSSFSheet ws = workbook.getSheet(WorkSheetName);
		int noOfColumns = ws.getRow(0).getPhysicalNumberOfCells();
		int rowNum = ws.getLastRowNum() + 1;
		for (i = 0; i < noOfColumns; i++) {
		    try {
			if (MatchFullString(ws.getRow(0).getCell(i).getStringCellValue(), ColumnName)) {
			    break;
			}
		    } catch (Exception e) {
		    }
		}
		for (int j = 0; j < rowNum; j++) {
		    try {
			if (MatchFullString(ws.getRow(j).getCell(i).getStringCellValue(), ColumnValue)) {
			    TotalExecutionCount++;
			}
		    } catch (Exception e) {
		    }
		}

		// @ Get Column Number.
		workbook.close();
		file.close();
		return TotalExecutionCount;
	    } catch (Exception e) {
		ErrDescription = "Unable to get worksheet count from Testsuite.xlsx file";
	    }
	} else {
	    ErrDescription = "Test Suite workbook is not found on following location - " + WorkSheetPath;
	}
	return TotalExecutionCount;
    }

    public String GetWorkSheetName(String WorkSheetPath, int Index) {
	killExcel();
	Path path = Paths.get(WorkSheetPath);
	String WorkSheetName = "";
	if (Files.exists(path)) {
	    try {
		FileInputStream file = new FileInputStream(new File(WorkSheetPath));
		XSSFWorkbook workbook = new XSSFWorkbook(file);
		WorkSheetName = workbook.getSheetName(Index);
		// @ Get Column Number.
		workbook.close();
		file.close();
		return WorkSheetName;
	    } catch (Exception e) {
		ErrDescription = "Unable to get worksheet count from Testsuite.xlsx file";
	    }
	} else {
	    ErrDescription = "Test Suite workbook is not found on following location - " + WorkSheetPath;
	}
	return WorkSheetName;
    }

    public boolean SaveTestSuiteValue(String WorkSheetPath, String WorkSheetName, int RowNumber) {
	killExcel();
	Path path = Paths.get(WorkSheetPath);
	int i = 0;
	int TotalExecutionCount = 0;
	if (Files.exists(path)) {
	    try {
		FileInputStream file = new FileInputStream(new File(WorkSheetPath));
		XSSFWorkbook workbook = new XSSFWorkbook(file);
		XSSFSheet ws = workbook.getSheet(WorkSheetName);
		int noOfColumns = ws.getRow(0).getPhysicalNumberOfCells();
		int rowNum = ws.getLastRowNum() + 1;
		for (i = 0; i < noOfColumns; i++) {
		    try {
			if (MatchFullString(ws.getRow(0).getCell(i).getStringCellValue(), "Run")) {
			    break;
			}
		    } catch (Exception e) {
		    }
		}
		for (int j = 0; j < rowNum; j++) {
		    try {
			if (MatchFullString(ws.getRow(j).getCell(i).getStringCellValue(), "Y")) {
			    TotalExecutionCount++;
			    if (RowNumber == TotalExecutionCount - 1) {

				for (int k = 0; k < noOfColumns; k++) {
				    try {
					dicTestSuite.put(ws.getRow(0).getCell(k).getStringCellValue().trim(), ws.getRow(j).getCell(k).getStringCellValue()
						.trim());
				    } catch (Exception e) {
				    }
				}
				break;
			    }

			}

		    } catch (Exception e) {
		    }
		}

		// @ Get Column Number.
		workbook.close();
		return true;
	    } catch (Exception e) {
		ErrDescription = "Unable to get worksheet count from Testsuite.xlsx file";
	    }
	} else {
	    ErrDescription = "Test Suite workbook is not found on following location - " + WorkSheetPath;
	}
	return false;
    }

    public boolean AddTestDataForApplication1(String TestId, String WorkSheetPath, String WorkSheetName, int RowNumber) {
	killExcel();
	
	if (MatchSubstringFromString(dicTestSuite.get("IterationType"), "Independent from test data")) {
	    dicTestData.put("TestCaseName", dicTestSuite.get("AutomatesTestCaseName"));
	    return true;
	}
	Path path = Paths.get(WorkSheetPath);
	int i = 0;
	int TotalExecutionCount = 0;
	if (Files.exists(path)) {
	    try {
		FileInputStream file = new FileInputStream(new File(WorkSheetPath));
		XSSFWorkbook workbook = new XSSFWorkbook(file);
		XSSFSheet ws = workbook.getSheet(WorkSheetName);
		int noOfColumns = ws.getRow(0).getPhysicalNumberOfCells();
		int rowNum = ws.getLastRowNum() + 1;
		for (i = 0; i < noOfColumns; i++) {
		    try {
			if (MatchFullString(ws.getRow(0).getCell(i).getStringCellValue(), "TestCaseID")) {
			    break;
			}
		    } catch (Exception e) {
		    }
		}
		for (int j = 0; j < rowNum; j++) {
		    try {
			if (MatchFullString(ws.getRow(j).getCell(i).getStringCellValue(), TestId)) {
			    TotalExecutionCount++;
			    if (RowNumber == TotalExecutionCount) {

				for (int k = 0; k < noOfColumns; k++) {
				    try {
					dicTestData.put(ws.getRow(0).getCell(k).getStringCellValue().trim(), ws.getRow(j).getCell(k).getStringCellValue()
						.trim());
				    } catch (Exception e) {
				    }
				}
				break;
			    }

			}

		    } catch (Exception e) {
		    }
		}

		// @ Get Column Number.
		workbook.close();
		file.close();
		return true;
	    } catch (Exception e) {
		ErrDescription = "Unable to get worksheet count from Testsuite.xlsx file";
	    }
	} else {
	    ErrDescription = "Test Suite workbook is not found on following location - " + WorkSheetPath;
	}
	return false;
    }

    public int TotalIterationCountForSingleTestCase(String WorkBookPath, String WorkSheetName, String TestCaseID) {
	killExcel();
	try {
	    String IterationType = dicTestSuite.get("IterationType");
	    switch (IterationType.toLowerCase().trim().replace(" ", "")) {
	    case "runfromrowtorow":
		String Rowcount = dicTestSuite.get("IterationValue");
		Rowcount = Rowcount.trim();
		String[] TotalCount = Rowcount.split("-");
		int totalrow = Integer.parseInt(TotalCount[1]) - Integer.parseInt(TotalCount[0]);
		return totalrow + 1;
	    case "runparticularrows":
		String TotalRowcount = dicTestSuite.get("IterationValue");
		TotalRowcount = TotalRowcount.trim();
		String[] TotalNumber = TotalRowcount.split(",");
		return TotalNumber.length;
	    case "independentfromtestdata":
		return 1;

	    }
	    return GetTotalRowExecutionCount(WorkBookPath, WorkSheetName, "TestCaseID", TestCaseID);
	} catch (Exception e) {
	}
	return 0;
    }

    public int GetRowNumberbasedonIterationValue(int IterationValue) {
	// @ Get Start Date
	killExcel();
	DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
	Calendar calobj = Calendar.getInstance();
	StartDateExecution = df.format(calobj.getTime()).toString();

	try {
	    String IterationType = dicTestSuite.get("IterationType");
	    switch (IterationType.toLowerCase().trim().replace(" ", "")) {
	    case "runfromrowtorow":
		String Rowcount = dicTestSuite.get("IterationValue");
		Rowcount = Rowcount.trim();
		String[] TotalCount = Rowcount.split("-");
		if (IterationValue > 0) {
		    return Integer.parseInt(TotalCount[0]) + IterationValue;
		}
		return Integer.parseInt(TotalCount[0]);
	    case "runparticularrows":
		String TotalRowcount = dicTestSuite.get("IterationValue");
		TotalRowcount = TotalRowcount.trim();
		String[] TotalNumber = TotalRowcount.split(",");
		return Integer.parseInt(TotalNumber[IterationValue]);
	    case "runonallrows":
		return IterationValue + 1;
	    case "independentfromtestdata":
		return 1;
	    }
	} catch (Exception e) {
	}
	return 0;
    }

    public String GetTempPath() {
	return System.getProperty("java.io.tmpdir");
    }

    public void SaveTestCaseStatus(String TestCaseID, String TestCaseName) {
	boolean blnFlag = false;
	String TestSuitePath = GetTempPath() + "AUT_Results\\TestSuite\\";
	blnFlag = CreateDirectory(TestSuitePath);
	if (blnFlag) {
	    try {
		Writer output;
		output = new BufferedWriter(new FileWriter(TestSuitePath + "TestSuiteSummary.txt", true));
		String Message = TestCaseID + " }" + TestCaseName + " }" + GetTestCaseStatus();
		String newLine = System.getProperty("line.separator");
		output.append(newLine + Message);
		output.close();
	    } catch (Exception e) {
	    }
	}

    }

    public void SaveHTMLReportsPath() {
	boolean blnFlag = false;
	String HTMLReportsLink = GetTempPath() + "AUT_Results\\Reports\\";
	blnFlag = CreateDirectory(HTMLReportsLink);
	if (blnFlag) {
	    try {
		Writer output;
		output = new BufferedWriter(new FileWriter(HTMLReportsLink + "HTMLRepotsLink.txt", true));
		String newLine = System.getProperty("line.separator");
		output.append(newLine + sReportFileName);
		output.close();
	    } catch (Exception e) {
	    }
	}
    }

    public void CreatingRepotingPackage(String TestCaseID, String TestCaseName) {
	killExcel();
	if (MatchFullString(dicConfig.get("EmailTrigger"), "Yes")) {
	    SaveTestCaseStatus(TestCaseID, TestCaseName);
	    SaveHTMLReportsPath();
	}

    }

    public String GetTestCaseStatus() {
	String Status = "Fail";
	if (iFailStepCount == 0) {
	    return "Pass";
	}
	return Status;
    }

    public String GetTestCaseStatusIteration() {
	String Status = "Fail";
	if (TotalFailStepCount == 0) {
	    return "Pass";
	}
	return Status;
    }

    public boolean CreateDirectory(String DirectoryName) {
	File file = new File(DirectoryName);
	if (!file.exists()) {
	    if (file.mkdirs()) {
		return true;
	    } else {
		ErrDescription = "Failed to create directory!";
		return false;
	    }
	}
	return true;
    }

    public void DeleteDirectory(String DirectoryName) {
	File directory = new File(DirectoryName);
	if (!directory.exists()) {
	    return;
	} else {
	    try {
		DeleteFolderContent(directory);
	    } catch (Exception e) {
		System.out.println("Exception");
	    }
	}

    }

    public void DeleteFolderContent(File file) {
	if (file.isDirectory()) {

	    // directory is empty, then delete it
	    if (file.list().length == 0) {
		file.delete();

	    } else {
		// list all the directory contents
		String files[] = file.list();

		for (String temp : files) {
		    // construct the file structure
		    File fileDelete = new File(file, temp);

		    // recursive delete
		    DeleteFolderContent(fileDelete);
		}

		// check the directory again, if empty then delete it
		if (file.list().length == 0) {
		    file.delete();
		}
	    }

	} else {
	    // if file, then delete it
	    file.delete();
	}
    }

    public void CreateTestSuiteSummaryHeader(String HTMLFileName) {
	try {
	    String TestSuitePath = GetTempPath() + "AUT_Results\\TestSuite\\";
	    String TestSuiteHTMLName = TestSuitePath + HTMLFileName + ".html";
	    oReportWriter = new FileOutputStream(TestSuiteHTMLName);
	    HtmlWriter = new OutputStreamWriter(oReportWriter);
	    HtmlWriter.write("<html>");
	    HtmlWriter.write("<style>");

	    HtmlWriter
		    .write(".subheading { BORDER-RIGHT:#000000 1px solid;PADDING-RIGHT: 4px;BORDER-TOP: #000000 1px solid;PADDING-LEFT: 4px;FONT-WEIGHT: bold;FONT-SIZE: 11pt;PADDING-BOTTOM: 0px;BORDER-LEFT: #000000 1px solid;COLOR: #000000;PADDING-TOP: 0px;BORDER-BOTTOM: #000000 1px solid;FONT-FAMILY: Consolas,helvetica, sans-serif;HEIGHT: 10px;BACKGROUND-COLOR: #FAC090;Color: #000000}");

	    HtmlWriter
		    .write(".subheading1{BORDER-RIGHT: #000000 1px solid;BACKGROUND-COLOR: #CCC0DA;PADDING-RIGHT: 4px;BORDER-TOP: #000000 1px solid;PADDING-LEFT: 4px;FONT-WEIGHT: bold;FONT-SIZE: 13pt;PADDING-BOTTOM: 0px;BORDER-LEFT: #000000 1px solid;COLOR: #000000;PADDING-TOP: 0px;BORDER-BOTTOM: #000000 1px solid;FONT-FAMILY: Consolas,helvetica, sans-serif;HEIGHT: 10px;}");

	    HtmlWriter
		    .write(".subheading2{BORDER-RIGHT: #000000 1px solid;PADDING-RIGHT: 2px;BORDER-TOP: #000000 1px solid;PADDING-LEFT: 2px;FONT-WEIGHT: bold;FONT-SIZE: 11pt;PADDING-BOTTOM: 0px;BORDER-LEFT: #000000 1px solid;COLOR: #000000;PADDING-TOP: 0px;BORDER-BOTTOM: #000000 1px solid;FONT-FAMILY: Consolas,helvetica, sans-serif;HEIGHT: 10px;BACKGROUND-COLOR: #C2DC9A;Color: #000000}");

	    HtmlWriter
		    .write(".tdborder_1{BORDER-RIGHT: #000000 1px solid;PADDING-RIGHT: 4px;BORDER-TOP: #000000 1px solid;PADDING-LEFT: 4px;FONT-SIZE: 11pt;PADDING-BOTTOM: 0px;BORDER-LEFT: #000000 1px solid;COLOR: #000000;PADDING-TOP: 0px;BORDER-BOTTOM: #000000 1px solid;FONT-FAMILY: Consolas, helvetica,  sans-serif;HEIGHT: 10px}");

	    HtmlWriter
		    .write(".tdborder_1_Pass{BORDER-RIGHT: #000000 1px solid;PADDING-RIGHT: 4px;BORDER-TOP: #000000 1px solid;PADDING-LEFT: 4px;FONT-SIZE: 11pt;PADDING-BOTTOM: 0px;BORDER-LEFT: #000000 1px solid;COLOR: #00ff00;PADDING-TOP: 0px;BORDER-BOTTOM: #000000 1px solid;FONT-FAMILY: Consolas,  helvetica, sans-serif;HEIGHT: 10px}");

	    HtmlWriter
		    .write(".SnapShotLink_style{PADDING-RIGHT: 4px;PADDING-LEFT: 4px;FONT-SIZE: 11pt;PADDING-BOTTOM: 0px;COLOR: #0000EE;PADDING-TOP: 0px;FONT-FAMILY: Consolas, helvetica, sans-serif;HEIGHT: 10px}");

	    HtmlWriter
		    .write(".tdborder_1_Fail{BORDER-RIGHT: #000000 1px solid;PADDING-RIGHT: 4px;BORDER-TOP: #000000 1px solid;PADDING-LEFT: 4px;FONT-SIZE: 11pt;PADDING-BOTTOM: 0px;BORDER-LEFT: #000000 1px solid; COLOR: #ff0000;PADDING-TOP: 0px;BORDER-BOTTOM: #000000 1px solid;FONT-FAMILY: Consolas, helvetica, sans-serif;HEIGHT: 10px}");

	    HtmlWriter
		    .write(".tdborder_1_Done{BORDER-RIGHT: #000000 1px solid;PADDING-RIGHT: 4px;BORDER-TOP: #000000 1px solid;PADDING-LEFT: 4px;FONT-SIZE: 11pt;PADDING-BOTTOM: 0px;BORDER-LEFT: #000000 1px solid; COLOR: #ffcc00;PADDING-TOP: 0px;BORDER-BOTTOM: #000000 1px solid;FONT-FAMILY: Consolas,  helvetica, sans-serif;HEIGHT: 10px}");

	    HtmlWriter
		    .write(".tdborder_1_Skipped{BORDER-RIGHT: #000000 1px solid;PADDING-RIGHT: 4px;BORDER-TOP: #000000 1px solid;PADDING-LEFT: 4px;FONT-SIZE: 11pt;PADDING-BOTTOM: 0px;BORDER-LEFT: #000000 1px  solid;COLOR: #00ccff;PADDING-TOP: 0px;BORDER-BOTTOM: #000000 1px solid;FONT-FAMILY: Consolas, helvetica, sans-serif;HEIGHT: 10px}");

	    HtmlWriter
		    .write(".tdborder_1_Warning{BORDER-RIGHT: #000000 1px solid;PADDING-RIGHT: 4px;BORDER-TOP: #000000 1px solid;PADDING-LEFT: 4px;FONT-SIZE: 11pt;PADDING-BOTTOM: 0px;BORDER-LEFT: #000000 1px solid;COLOR: #660066;PADDING-TOP: 0px;BORDER-BOTTOM: #000000 1px solid;FONT-FAMILY: Consolas, helvetica, sans-serif;HEIGHT: 10px}");

	    HtmlWriter.write(".heading {FONT-WEIGHT: bold; FONT-SIZE: 17px; COLOR: #005484;FONT-FAMILY: Consolas, Verdana, Tahoma, Consolas;}");

	    HtmlWriter
		    .write(".style1 { border: 1px solid #8eb3d8;padding: 0px 4px;FONT-WEIGHT: bold;FONT-SIZE: 11pt;COLOR: #000000;FONT-FAMILY: Consolas, helvetica, sans-serif;HEIGHT: 10px;width: 180px;}");

	    HtmlWriter
		    .write(".style3 { border: 1px solid #8eb3d8;padding: 0px 4px;FONT-WEIGHT: bold;FONT-SIZE: 11pt;COLOR: #000000;FONT-FAMILY: Consolas, helvetica, sans-serif;HEIGHT: 10px;width: 2px;}");

	    HtmlWriter.write("</style>");

	    HtmlWriter.write("<head><title>" + dicConfig.get("ProjectName") + " Test Summary Results</title></head>");

	    HtmlWriter.write("<body>");

	    HtmlWriter.write("<table cellSpacing='0' cellPadding='0' border='0' align='center' style='width:64%;"
		    + " margin-left:10px;'><td class='subheading1' colspan=5 align=center><p style='font-size:1.8em'>" + "<body link='#00ff00'>"
		    + dicConfig.get("ProjectName") + " Test Summary Results</body></td><tr></tr></table>");

	    // @ add column

	    HtmlWriter.write("<table cellSpacing='0' cellPadding='0' border='0' align='center' style='width:64%; margin-" + "left:10px;'>");

	    HtmlWriter.write("</table>");

	    HtmlWriter.write("<table cellSpacing='0' cellPadding='0' border='0' align='center' style='width:64%;" + " margin-left:10px;'>");

	    HtmlWriter.write("<tr></tr>");

	    HtmlWriter.write("<tr>" + " <td class='subheading2' width ='4%' align='center'>S.no</td>"
		    + " <td class='subheading2'  width ='4%' align='center'>TestID</td>"
		    + " <td class='subheading2'  width ='31%' align='center'>Test Case Name</td>"
		    + " <td class='subheading2' width ='4%' align='center'>Status</td>"
		    + " <td class='subheading2' width ='21%' align='center'>Detail Report link</td>" + " </tr>");
	} catch (Exception e) {
	}
    }

    public void CreateTestSuiteSummaryReports(String HtmlfileName) {
	// @ creating Header
	killExcel();
	if (MatchFullString(dicConfig.get("EmailTrigger"), "Yes")) {
	    CreateTestSuiteSummaryHeader(HtmlfileName);
	    String DYLinkName = "";
	    // @ Read Text File
	    try {

		String TestSuiteTextFile = GetTempPath() + "AUT_Results\\TestSuite\\TestSuiteSummary.txt";
		LineIterator it = IOUtils.lineIterator(new BufferedReader(new FileReader(TestSuiteTextFile)));
		for (int lineNumber = 0; it.hasNext(); lineNumber++) {
		    String line = (String) it.next();
		    if (line.trim().contains("}")) {
			String[] comp = line.split("}");
			String TestCaseID = comp[0].trim();
			String TestCaseName = comp[1].trim();
			String Status = comp[2].trim();
			DYLinkName = GetHtmlLocation(lineNumber);
			FeedTestCaseStatusinTestSuite(lineNumber, TestCaseID, TestCaseName, Status, DYLinkName);
			// @ call
		    }
		}

		// Close Test Suite Reports.

		HtmlWriter.write("</table></body></html>");
		HtmlWriter.close();
		// oReportWriter.Flush();
	    } catch (Exception e) {
	    }

	    SendEmail();
	}
    }

    public String GetHtmlLocation(int lineNumber) {
	String Link = "";
	int LineNumber = 0;
	try {

	    String HtmlLinkLocation = GetTempPath() + "AUT_Results\\Reports\\HTMLRepotsLink.txt";
	    LineIterator it = IOUtils.lineIterator(new BufferedReader(new FileReader(HtmlLinkLocation)));
	    for (LineNumber = 0; it.hasNext(); LineNumber++) {
		String line = (String) it.next();
		if (LineNumber == lineNumber) {
		    return line;
		}
	    }
	} catch (Exception e) {
	}
	return Link;
    }

    public void FeedTestCaseStatusinTestSuite(int SNo, String TestCaseID, String Testcasename, String Status, String DYLinkName) {
	// @ add test case
	try {
	    HtmlWriter.write("<tr>");
	    HtmlWriter.write("<TD class='tdborder_1' width ='4%' align='center' >" + SNo + "</TD>");
	    HtmlWriter.write("<td class ='tdborder_1' width ='4%' >" + TestCaseID + "</td>");
	    HtmlWriter.write("<td class ='tdborder_1' width ='31%' >" + Testcasename + "</td>");
	    if (MatchSubstringFromString(Status, "pass")) {
		HtmlWriter.write(" <td  class ='tdborder_1_Pass' width ='4%'align='center' >" + Status + "</td>");
	    } else {
		HtmlWriter.write(" <td  class ='tdborder_1_Fail' width ='4%' align='center' >" + Status + "</td>");
	    }
	    String scrennshotlink = "file:///" + DYLinkName;
	    String linkName = "Click here for detail Report";

	    HtmlWriter.write(" <td > <a class ='tdborder_1 width ='21%' align='center' href=" + scrennshotlink + ">" + linkName + "</a></TD>");
	} catch (Exception e) {
	}

	// @ end Test Case
    }

    public void DeleteSpecificTextFile() {
	try {
	    String TestSuitePath = GetTempPath() + "AUT_Results\\TestSuite\\TestSuiteSummary.txt";
	    String LinkPath = GetTempPath() + "AUT_Results\\Reports\\HTMLRepotsLink.txt";
	    File oldFile = new File(TestSuitePath);
	    oldFile.delete();
	    File oldFile1 = new File(LinkPath);
	    oldFile1.delete();
	} catch (Exception e) {
	}
    }

    public boolean UpdateTestSuiteWorkBookWithStatus(String WorkSheetPath, String WorkSheetName, int RowNumber, int ToTalScriptCount) {
	killExcel();
	String Status = GetTestCaseStatusIteration();
	Path path = Paths.get(WorkSheetPath);
	int i = 0;
	int TotalExecutionCount = 0;
	if (Files.exists(path)) {
	    try {
		FileInputStream file = new FileInputStream(new File(WorkSheetPath));
		XSSFWorkbook workbook = new XSSFWorkbook(file);
		XSSFSheet ws = workbook.getSheet(WorkSheetName);
		int noOfColumns = ws.getRow(0).getPhysicalNumberOfCells();
		int rowNum = ws.getLastRowNum() + 1;
		for (i = 0; i < noOfColumns; i++) {
		    try {
			if (MatchFullString(ws.getRow(0).getCell(i).getStringCellValue(), "Run")) {
			    break;
			}
		    } catch (Exception e) {
		    }
		}
		for (int j = 0; j < rowNum; j++) {
		    try {
			if (MatchFullString(ws.getRow(j).getCell(i).getStringCellValue(), "Y")) {
			    TotalExecutionCount++;
			    if (RowNumber == TotalExecutionCount - 1) {

				for (int k = 0; k < noOfColumns; k++) {
				    try {
					String ColumnName = ws.getRow(0).getCell(k).getStringCellValue().trim();

					switch (ColumnName.toLowerCase().trim().replace(" ", "")) {
					case "status":
					    ws.getRow(j).getCell(k).setCellType(org.apache.poi.ss.usermodel.Cell.CELL_TYPE_STRING);
					    ws.getRow(j).getCell(k).setCellValue(Status);
					    Cell cell1 = ws.getRow(j).getCell(k);
					    CellStyle style = workbook.createCellStyle();
					    if (MatchFullString(Status, "Pass")) {
						style.setFillBackgroundColor(IndexedColors.GREEN.getIndex());
					    } else {
						style.setFillBackgroundColor(IndexedColors.RED.getIndex());
					    }
					    style.setFillPattern(CellStyle.ALIGN_FILL);
					    cell1.setCellStyle(style);
					    break;
					case "reportslink":
					    ws.getRow(j).getCell(k).setCellType(org.apache.poi.ss.usermodel.Cell.CELL_TYPE_STRING);
					    XSSFCreationHelper helper = workbook.getCreationHelper();
					    XSSFHyperlink file_link = helper.createHyperlink(XSSFHyperlink.LINK_FILE);
					    String LinkAddress = sReportFileName.replace("\\", "/");
					    if (ToTalScriptCount > 1) {
						LinkAddress = LinkAddress.split("/" + dicTestSuite.get("TestCaseID"))[0] + "/" + dicTestSuite.get("TestCaseID");
					    }
					    file_link.setAddress("file:///" + LinkAddress);
					    file_link.setTooltip("Click to open the file");
					    ws.getRow(j).getCell(k).setCellValue("Click here for " + dicConfig.get("ReportingFormat") + " Report");
					    ws.getRow(j).getCell(k).setHyperlink(file_link);
					    break;
					}
				    } catch (Exception e) {
				    }
				}
				break;
			    }

			}

		    } catch (Exception e) {
		    }
		}

		// @ Get Column Number.
		// lets write the excel data to file now
		FileOutputStream fos = new FileOutputStream(new File(WorkSheetPath));
		workbook.write(fos);
		fos.close();
		workbook.close();
		file.close();
		return true;
	    } catch (Exception e) {
		ErrDescription = "Unable to get worksheet count from Testsuite.xlsx file";
	    }
	} else {
	    ErrDescription = "Test Suite workbook is not found on following location - " + WorkSheetPath;
	}
	return false;

    }

    public String GetTimeDiffernce(String dateStart, String dateStop) {
	String Difference = "";
	SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
	try {
	    java.util.Date dt1 = format.parse(dateStart);
	    java.util.Date dt2 = format.parse(dateStop);
	    long diff = dt2.getTime() - dt1.getTime();
	    long diffSeconds = TimeUnit.MILLISECONDS.toSeconds(diff) % 60;
	    long diffMinutes = TimeUnit.MILLISECONDS.toMinutes(diff) % 60;
	    long diffHours = TimeUnit.MILLISECONDS.toHours(diff) % 24;
	    String Hour = "";
	    String Minutes = "";
	    String Second = "";
	    if (diffHours < 10) {
		Hour = "0" + diffHours;
	    } else {
		Hour = String.valueOf(diffHours);
	    }
	    if (diffMinutes < 10) {
		Minutes = "0" + diffMinutes;
	    } else {
		Minutes = String.valueOf(diffMinutes);
	    }
	    if (diffSeconds < 10) {
		Second = "0" + diffSeconds;
	    } else {
		Second = String.valueOf(diffSeconds);
	    }
	    Difference = Hour + ":" + Minutes + ":" + Second;
	} catch (Exception e) {
	}
	return Difference;
    }

    public void SendEmail() {
	// @ Credential For Email .
	final String username = dicConfig.get("SenderEmail");
	final String password = dicConfig.get("OfficePassword");

	// @------ Set SMTP mail properties--------------
	Properties props = new Properties();
	props.put("mail.smtp.auth", "true");
	props.put("mail.smtp.starttls.enable", "true");
	props.put("mail.smtp.host", dicConfig.get("SMTPServerDomain"));
	props.put("mail.smtp.port", "587");
	Session session = Session.getInstance(props, new javax.mail.Authenticator() {
	    protected PasswordAuthentication getPasswordAuthentication() {
		return new PasswordAuthentication(username, password);
	    }
	});
	try {
	    // @ Set Sender and Receiver Email Address
	    Message message = new MimeMessage(session);
	    message.setFrom(new InternetAddress(username));
	    String[] to = dicConfig.get("ReciverEmail").split(",");
	    InternetAddress[] addressTo = new InternetAddress[to.length];
	    for (int i = 0; i < to.length; i++) {
		addressTo[i] = new InternetAddress(to[i]);
	    }
	    message.setRecipients(RecipientType.TO, addressTo);

	    // @ Set Subject Line
	    message.setSubject(dicConfig.get("EmailSubject"));

	    // @ ------ Get Html Body
	    // Text------------------------------------------
	    String HTMLText = ChangeHTMLBodyContent();

	    // @------ Define Html Body Type-----------------------------------
	    Multipart mp = new MimeMultipart();
	    Multipart multipart = new MimeMultipart();
	    MimeBodyPart htmlPart = new MimeBodyPart();
	    htmlPart.setContent(HTMLText, "text/html");
	    mp.addBodyPart(htmlPart);
	    message.setContent(mp);

	    // @ Set Attachment
	    String file = GetTempPath() + "AUT_Results\\TestSuite\\TestSuiteSummary.html";
	    String fileName = "TestSuiteSummary.html";
	    MimeBodyPart messageBodyPart2 = new MimeBodyPart();
	    DataSource source = new FileDataSource(file);
	    messageBodyPart2.setDataHandler(new DataHandler(source));
	    messageBodyPart2.setFileName(fileName);
	    mp.addBodyPart(messageBodyPart2);

	    // @ Given sent Command.
	    Transport.send(message);

	    System.out.println("Sent");
	} catch (MessagingException e) {
	    e.printStackTrace();
	}
    }

    public String ReadAllTextFromFile(String FilePath) {
	String Text = "";
	try {
	    Text = new String(Files.readAllBytes(Paths.get(FilePath)));
	} catch (Exception e) {
	}
	return Text;
    }

    public String ChangeHTMLBodyContent() {
	SimpleDateFormat df = new SimpleDateFormat("dd MMM yyyy");
	Calendar calobj = Calendar.getInstance();
	String DateForExecution = df.format(calobj.getTime()).toString();
	String HTMLBodyText = "";
	String TestSummaryText = ReadAllTextFromFile(GetTempPath() + "AUT_Results\\TestSuite\\TestSuiteSummary.txt");
	int Pass = GetSubstringCount(TestSummaryText, "pass");
	int Fail = GetSubstringCount(TestSummaryText, "Fail");
	int Total = Pass + Fail;
	long passc = (Pass * 100) / Total;
	long failc = (Fail * 100) / Total;

	HTMLBodyText = ReadAllTextFromFile(dicInput.get("MiscPath") + "ReportTemplate\\HTMLReportTemplate.html");
	HTMLBodyText = HTMLBodyText.replace("Change9", dicConfig.get("ProjectName")).replace("Change7", dicConfig.get("BrowserType"))
		.replace("Change8", dicConfig.get("Environment"));
	HTMLBodyText = HTMLBodyText.replace("Change2", Integer.toString(Pass)).replace("Change3", Integer.toString(Fail))
		.replace("Change4", Integer.toString(Total));
	HTMLBodyText = HTMLBodyText.replace("Change5", String.valueOf(passc)).replace("Change6", String.valueOf(failc)).replace("Change1", DateForExecution);
	return HTMLBodyText;
    }

    public int GetSubstringCount(String set, String subset) {
	int Result = 0;
	try {
	    Result = (set.toLowerCase().length() - set.toLowerCase().replace(subset.toLowerCase(), "").length()) / subset.length();
	    return Result;
	} catch (Exception e) {
	    return Result;
	}
    }

    public void GetRunningApplicationName() {
	String s = null;

	try {

	    Runtime.getRuntime().exec("taskkill /F /IM Config.xlsx");
	} catch (Exception e) {
	    System.out.println("exception happened - here's what I know: ");

	}
    }

    public void killExcel() {
	try {
	    Runtime.getRuntime().exec("taskkill /IM EXCEL.EXE");
	} catch (Exception e) {
	}
    }

}
