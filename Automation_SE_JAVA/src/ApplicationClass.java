
public class ApplicationClass extends GenericClass
{

    //########################################################################################################
    //# Function ID : 
    //# Function Name: VerifyHomePage
    //#------------------------------------------------------------------------------------------------------
    //# Description: This Function Verify Home page for Google.
    //#------------------------------------------------------------------------------------------------------
    //# Argument : NA
    //# 
    //#------------------------------------------------------------------------------------------------------
    //# Owner:  Cyber Group
    //# Created on: 
    //#------------------------------------------------------------------------------------------------------
    //# Reviewer: 
    //# Review Date: 
    //#------------------------------------------------------------------------------------------------------
    //# History Notes: 
    //#------------------------------------------------------------------------------------------------------
    //# Example : VerifyHomePage()
    //#------------------------------------------------------------------------------------------------------
    //# Return type : true/false
    //########################################################################################################

    public boolean VerifyHomePage()
    {
    	boolean blnFlag = false;
        try
        {
        	blnFlag = OpenBrowser(dicCommonValue.get("ApplicatonURL"),dicConfig.get("BrowserType"));
            blnFlag = ExecuteActionOnPage("Pageelement","exist", "namfq","",1);
            if (blnFlag)
            {
                ErrDescription = "Google Home page is displayed after launching URL - " + dicCommonValue.get("ApplicatonURL") + " from browser - " + dicConfig.get("BrowserType") + ".";
                return true;
            }
            else
            {
                ErrDescription = "Google Home page is not displayed after launching URL - " + dicCommonValue.get("ApplicatonURL") + " from browser - " + dicConfig.get("BrowserType") + ".";
                return false;
            }
        }
        catch(Exception ee)
        {
            ErrDescription = "Unable to launch Application - " + dicCommonValue.get("ApplicatonURL")+ " from browser - " + dicConfig.get("BrowserType")+ ".";
            return false;
        }
    }
    
    public boolean SearchKeywordinGoogle(String SearchItem)
    {
    	boolean blnFlag = false;
        blnFlag = ExecuteActionOnPage("pageelement", "set", "namfq", SearchItem);
        if (blnFlag)
        {
        	WaitForObject("pageelement","namfbtnG",10);
            blnFlag = ExecuteActionOnPage("pageelement", "click", "namfbtnG");
            if (blnFlag)
            {
            	WaitForObject("pageelement","idefresultStats",10);
	                blnFlag = VerifyObjectText("idefresultStats", "About");
                if (blnFlag)
                {
                    ErrDescription = "Search Result page - "+dicOutput.get("strOutPut")+" is  displayed after searching keyword - '" + SearchItem + "'.";
                }
                else
                {
                    ErrDescription = "Search Result page is not displayed after searching keyword - '" + SearchItem + "'.";
                }
            }
            else
            {
                ErrDescription = "'Google search button' icon not displayed on Top of the page.";
            }
        }
        else
        {
            ErrDescription = "'Google search editbox' is not displayed on 'Google' Home page.";
        }
        return blnFlag;
    }
}
