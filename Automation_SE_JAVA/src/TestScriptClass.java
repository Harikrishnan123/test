import java.io.File;
import java.util.Arrays;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;


public class TestScriptClass extends ApplicationClass
{
    //########################################################################################################
    //# Test Case ID: 1 
    //# Test Case Name: Search any Item on Google Home page.
    //#------------------------------------------------------------------------------------------------------
    //# Description: This test case verify search functionality on Google home page
    //#------------------------------------------------------------------------------------------------------
    //# Pre-conditions: NA
    //# Post-conditions: NA
    //# Limitations: NA
    //#------------------------------------------------------------------------------------------------------
    //# Owner:  Pravesh Jha
    //# Created on: 24-Feb-2014
    //#------------------------------------------------------------------------------------------------------
    //# Reviewer: 
    //# Review Date: 
    //#------------------------------------------------------------------------------------------------------
    //# History Notes: 
    //########################################################################################################

    public void GoogleSearchFunctionality()
    {

        //#------------------------------------------------------------------------------------------------------
        //@ Variables declaration & initialization
        String strStepDesc = "";
        boolean blnStepRC = false;
       String SearchItem = dicTestData.get("SearchItem");

        //########################################################################################################
        //Step 1 - 	Open browser and go to www.nerium.com
        //########################################################################################################

        strStepDesc = "Verify Home Page.";

        blnStepRC = VerifyHomePage();
        if (blnStepRC)
        {
        	ReportStep(strStepDesc, ErrDescription, "Pass");
        }
        else
        {
        	ReportStep(strStepDesc, ErrDescription, "Fail");
        }


        //#------------------------------------------------------------------------------------------------------------
        
        //########################################################################################################
        //Step 2 - 	Click 'Science' link at top and verify that page redirects to 'Science' page.
        //########################################################################################################
        
        //@ Practice
        strStepDesc = "Search Keyword - '" + SearchItem+ "' on Google.";

        blnStepRC = SearchKeywordinGoogle(SearchItem);
        if (blnStepRC)
        {
        	ReportStep(strStepDesc, ErrDescription, "Pass");
        }
        else
        {
        	ReportStep(strStepDesc, ErrDescription, "Fail");
        }
        
        //#------------------------------------------------------------------------------------------------------------
        //########################################################################################################
        //Step 2 - 	Click 'Science' link at top and verify that page redirects to 'Science' page.
        //########################################################################################################
        
        //@ Practice
        strStepDesc = "Get Total Link Count - '" + SearchItem+ "' page.";

        blnStepRC = ExecuteActionOnPage("Pageelement","Count","//a");
        if (blnStepRC)
        {
        	ReportStep(strStepDesc, "Total Link count is "+ dicOutput.get("strOutPut") + " .", "Pass");
        }
        else
        {
        	ReportStep(strStepDesc, "No any link is found on this page.", "Fail");
        }
    }

        //#-----------------------------------------------------------------------------------------------------------------------------
        
        //########################################################################################################
        //# Test Case ID: 2 
        //# Test Case Name: Search any Item on Google Home page.
        //#------------------------------------------------------------------------------------------------------
        //# Description: This test case verify search functionality on Google home page
        //#------------------------------------------------------------------------------------------------------
        //# Pre-conditions: NA
        //# Post-conditions: NA
        //# Limitations: NA
        //#------------------------------------------------------------------------------------------------------
        //# Owner:  Pravesh Jha
        //# Created on: 24-Feb-2014
        //#------------------------------------------------------------------------------------------------------
        //# Reviewer: 
        //# Review Date: 
        //#------------------------------------------------------------------------------------------------------
        //# History Notes: 
        //########################################################################################################

        public void SearchByVoiceFunctionality()
        {
            //#------------------------------------------------------------------------------------------------------
            //@ Variables declaration & initialization
            String strStepDesc = "";
            boolean blnStepRC = false;
           String SearchItem = dicTestData.get("SearchItem");

            //########################################################################################################
            //Step 1 - 	Open browser and go to www.nerium.com
            //########################################################################################################

            strStepDesc = "Verify Home Page.";

            blnStepRC = VerifyHomePage();
            if (blnStepRC)
            {
            	ReportStep(strStepDesc, ErrDescription, "Pass");
            }
            else
            {
            	ReportStep(strStepDesc, ErrDescription, "Fail");
            }


            //#------------------------------------------------------------------------------------------------------
            //########################################################################################################
            //Step 2 - 	Click 'Science' link at top and verify that page redirects to 'Science' page.
            //########################################################################################################
            
            //@ Practice
            strStepDesc = "Click on 'I m Feeling Button' icon.";

            blnStepRC = ExecuteActionOnPage("Pageelement","click","namfbtnI");
            if (blnStepRC)
            {
            	ReportStep(strStepDesc, "I M feeling Lucky button is displayed.", "Pass");
            }
            else
            {
            	ReportStep(strStepDesc, "Search By Voice Icon is not displayed on Google Editbox.", "Fail");
            }

            //#-----------------------------------------------------------------------------------------------------------------------------
        }
        
        //#-----------------------------------------------------------------------------------------------------------------------------
        
        //########################################################################################################
        //# Test Case ID: 2 
        //# Test Case Name: Search any Item on Google Home page.
        //#------------------------------------------------------------------------------------------------------
        //# Description: This test case verify search functionality on Google home page
        //#------------------------------------------------------------------------------------------------------
        //# Pre-conditions: NA
        //# Post-conditions: NA
        //# Limitations: NA
        //#------------------------------------------------------------------------------------------------------
        //# Owner:  Pravesh Jha
        //# Created on: 24-Feb-2014
        //#------------------------------------------------------------------------------------------------------
        //# Reviewer: 
        //# Review Date: 
        //#------------------------------------------------------------------------------------------------------
        //# History Notes: 
        //########################################################################################################

        public void GoogleAppCheckFunctionality()
        {
            //#------------------------------------------------------------------------------------------------------
            //@ Variables declaration & initialization
            String strStepDesc = "";
            boolean blnStepRC = false;
           String SearchItem = dicTestData.get("SearchItem");

            //########################################################################################################
            //Step 1 - 	Open browser and go to www.nerium.com
            //########################################################################################################

            strStepDesc = "Verify Home Page.";

            blnStepRC = VerifyHomePage();
            if (blnStepRC)
            {
            	ReportStep(strStepDesc, ErrDescription, "Pass");
            }
            else
            {
            	ReportStep(strStepDesc, ErrDescription, "Fail");
            }


            //#------------------------------------------------------------------------------------------------------
            //########################################################################################################
            //Step 2 - 	Click 'Science' link at top and verify that page redirects to 'Science' page.
            //########################################################################################################
            
            //@ Practice
            strStepDesc = "Click on 'App' icon.";

            blnStepRC = ExecuteActionOnPage("Pageelement","click","titfApps");
            if (blnStepRC)
            {
            	blnStepRC =VerifyObjectText("rolfregion",SearchItem);
            	if (blnStepRC)
                {
                	ReportStep(strStepDesc, SearchItem+ " App is  displayed on Google App flyout.", "Pass");
                }
                else
                {
                	ReportStep(strStepDesc, SearchItem+ " App is not displayed on Google App flyout.", "Fail");
                }
            	
            }
            else
            {
            	ReportStep(strStepDesc, "App icon is not displayed on Top section.", "Fail");
            }

            //#-----------------------------------------------------------------------------------------------------------------------------
        }
        
        public void SameScript_Java_VS()
        {
            //#------------------------------------------------------------------------------------------------------
            //@ Variables declaration & initialization
            String strStepDesc = "";
            boolean blnStepRC = false;

            //########################################################################################################
            //Step 1 - 	Open browser and go to www.nerium.com
            //########################################################################################################

            strStepDesc = "Verify Google Home Page.";
            blnStepRC = VerifyHomePage();
            if (blnStepRC)
            {
                ReportStep(strStepDesc, ErrDescription, "Pass");
            }
            else
            {
                ReportStep(strStepDesc, ErrDescription, "Fail");
            }

            //########################################################################################################
            //Step 1 - 	Search anyKeyword on Google Home Page
            //########################################################################################################

            strStepDesc = "Search anyKeyword on Google Home Page";
            blnStepRC = ExecuteActionOnPage("Pageelement", "set", "namfq", "java vs c# which is better");
            if (blnStepRC)
            {
                WaitForObject("Pageelement", "lnkpShould I Learn Java or C#");
                blnStepRC = ExecuteActionOnPage("Pageelement", "click", "lnkpShould I Learn Java or C#");
                if (blnStepRC)
                {
                    ReportStep(strStepDesc, "You Tube Page open succesfully", "Pass");
                }
                else
                {
                    ReportStep(strStepDesc, "You Tube Page open succesfully.", "Fail");
                }
            }
            else
            {
                ReportStep(strStepDesc, "Unable To Set Keyword.", "Fail");
            }

        }
        
        public void kholsSearch()
        {
            //#------------------------------------------------------------------------------------------------------
            //@ Variables declaration & initialization
            String strStepDesc = "";
            boolean blnStepRC = false;

            //########################################################################################################
            //Step 1 - 	Open browser and go to www.nerium.com
            //########################################################################################################

            strStepDesc = "Verify Google Home Page.";
            blnStepRC = VerifyHomePage();
            if (blnStepRC)
            {
                ReportStep(strStepDesc, ErrDescription, "Pass");
            }
            else
            {
                ReportStep(strStepDesc, ErrDescription, "Fail");
            }

            //########################################################################################################
            //Step 1 - 	Search anyKeyword on Google Home Page
            //########################################################################################################

            strStepDesc = "Search anyKeyword on Google Home Page";
            blnStepRC = ExecuteActionOnPage("Pageelement", "set", "namfq", "java vs c# which is better");
            if (blnStepRC)
            {
                WaitForObject("Pageelement", "lnkpShould I Learn Java or C#");
                blnStepRC = ExecuteActionOnPage("Pageelement", "click", "lnkpShould I Learn Java or C#");
                if (blnStepRC)
                {
                    ReportStep(strStepDesc, "You Tube Page open succesfully", "Pass");
                }
                else
                {
                    ReportStep(strStepDesc, "You Tube Page open succesfully.", "Fail");
                }
            }
            else
            {
                ReportStep(strStepDesc, "Unable To Set Keyword.", "Fail");
            }

        }
        
        //End of Class
    }
    



